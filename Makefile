makefile:
all: boffert

clean:
	rm -f boffert

boffert: boffert.c
	gcc boffert.c -o boffert -fno-stack-protector -no-pie -m32 -O0
