FROM ubuntu:18.04
ARG DEBIAN_FRONTEND=noninteractive
RUN dpkg --add-architecture i386
RUN apt update
RUN apt -y install socat libc6:i386 libstdc++6:i386 multiarch-support
RUN mkdir /app
WORKDIR /app
COPY boffert /app
COPY flag.txt /app
RUN useradd -ms /bin/bash boffert
RUN chmod 655 /app/*
USER boffert
ENTRYPOINT ["/usr/bin/socat", "tcp4-listen:4444,fork,reuseaddr", "EXEC:./boffert"]

