Basic buffer overflow challenge.

`make clean; make boffert` to (re)build binary.

Should run fine under socat or tcpserver.
