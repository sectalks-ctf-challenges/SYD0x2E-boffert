#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void bin_sh() {
        system("/bin/cat ./flag.txt");
}

int run() {
        char buffer[4];
        char printable_buffer[28];

        printf("Which characters would you like to place inside our 4 byte buffer? ");
        fgets(printable_buffer, 28, stdin);

        strcpy(buffer, printable_buffer);

        return 0;
}

int main(int argc, char const *argv[]) {
        printf(R"EOF(

                                .---.
                               @ @   )
                               ^     |
                              [|]    | ##
                              /      |####
                             (       |####
                              \| /   |#BP#
                             / |.'   |###
                            _\ ``\   )##
                            /,,_/,,____#

    _______  _______  _______         ____   _______  ____
   |  _    ||       ||       |       |    | |  _    ||    |
   | |_|   ||   _   ||    ___| ____   |   | | | |   | |   |
   |       ||  | |  ||   |___ |____|  |   | | | |   | |   |
   |  _   | |  |_|  ||    ___|        |   | | |_|   | |   |
   | |_|   ||       ||   |            |   | |       | |   |
   |_______||_______||___|            |___| |_______| |___|

)EOF");

        return run();
}
